const mutations = {
  setMonitors (state, monitors) {
    state.monitors = monitors
  }
}

export default mutations
