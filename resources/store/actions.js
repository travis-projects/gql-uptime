const actions = {
  async nuxtServerInit({ commit }, { app }) {
    if (app.$auth.loggedIn) {
      const data = await app.$axios.$get('api/monitors')
      commit('setMonitors', data)
    }
  },
  async fetchMonitors ({ commit }) {
    const data = await this.$axios.$get('api/monitors')
    commit('setMonitors', data)
  },
  async createMonitor ({ state, commit }, monitor) {
    const monitors = state.monitors
    await monitors.unshift(monitor)
    commit('setMonitors', monitors)
  },
  async removeMonitor ({ state, commit }, monitorId) {
    const monitors = state.monitors.filter(monitor => monitor.id !== monitorId)
    commit('setMonitors', monitors)
  }
}

export default actions
