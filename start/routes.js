'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/guides/routing
|
*/

const Route = use('Route')

Route
  .group(() => {
    Route.resource('user', 'UserController').apiOnly().middleware(['auth:jwt'])
    Route.resource('monitors', 'MonitorController').apiOnly().middleware(['auth:jwt'])
  })
  .prefix('api')

Route
.group(() => {
  Route.post('signup', 'AuthController.signup')
  Route.post('login', 'AuthController.login')
})
.prefix('api/auth')

Route.get('api/update', 'UpdateController.index')

Route.any('*', 'NuxtController.render')
