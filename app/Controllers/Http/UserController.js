'use strict'

class UserController {
  async index ({ auth, response }) {
    const user = await auth.getUser()
    return response.json({ user })
  }
}

module.exports = UserController
