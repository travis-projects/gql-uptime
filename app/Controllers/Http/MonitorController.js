'use strict'

const Monitor = use('App/Models/Monitor')

class MonitorController {
  async index ({ auth, response }) {
    const { id: userId } = await auth.getUser()
    try {
      const monitors = await Monitor.query().where('user_id', userId).with('checks').orderBy('created_at', 'desc').fetch()
      return response.json(monitors.toJSON())
    } catch (error) {
      return response.status(400).json({
        status: 'error',
        message: error.message
      })
    }
  }

  async store ({ request, auth, response }) {
    const { id: userId } = await auth.getUser()
    const monitorData = request.only(['name', 'type', 'url', 'query', 'is_active'])
    try {
      const monitor = await Monitor.create({
        user_id: userId,
        ...monitorData
      })

      return response.json(monitor)
    } catch (error) {
      return response.status(400).json({
        status: 'error',
        message: error.message
      })
    }
  }

  async show ({ params, auth, response }) {
    const { id: userId } = await auth.getUser()
    try {
      const monitor = await Monitor.query().where('user_id', userId).where('id', params.id).with('checks').firstOrFail()
      return response.json(monitor)
    } catch (error) {
      return response.status(400).json({
        status: 'error',
        message: 'Monitor Not Found'
      })
    }
  }

  async update ({ request, params, auth, response }) {
    const { id: userId } = await auth.getUser()
    const monitorData = request.only(['name', 'type', 'url', 'query', 'is_active'])
    try {
      const monitor = await Monitor.query().where('user_id', userId).where('id', params.id).update(monitorData)
      return response.json(monitor)
    } catch (error) {
      return response.status(400).json({
        status: 'error',
        message: error.message
      })
    }
  }

  async destroy ({ params, auth, response }) {
    const { id: userId } = await auth.getUser()
    try {
      await Monitor.query().where('user_id', userId).where('id', params.id).delete()
      return response.json({ status: 'deleted' })
    } catch (error) {
      console.log(error)
      return response.status(400).json({
        status: 'error',
        message: error.message
      })
    }
  }
}

module.exports = MonitorController
