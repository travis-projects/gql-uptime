'use strict'

const { request: client } = require('graphql-request')
const gql = require('graphql-tag')

const Monitor = use('App/Models/Monitor')
const Check = use('App/Models/Check')

class UpdateController {
  async index ({ request, response }) {
    const monitors = await Monitor.query().where('is_active', 1).with('user').fetch()
    try {
      await monitors.toJSON().forEach(async monitor => {
        try {
          const parsedQuery = gql`${monitor.query}`
          const response = await client(monitor.url, parsedQuery).then((data) => Check.create({
            monitor_id: monitor.id,
            status: 'up'
          })).catch((error) => Check.create({
            monitor_id: monitor.id,
            status: 'down'
          }))
          return response
        } catch (error) {
          console.error(error)
        }
      })
      response.send(200)
    } catch (error) {
      return response.status(400).json({
        status: 'error',
        message: error.message
      })
    }
  }
}

module.exports = UpdateController
