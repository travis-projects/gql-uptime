'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Monitor extends Model {
  user () {
    return this.belongsTo('App/Models/User')
  }

  checks () {
    return this.hasMany('App/Models/Check')
  }
}

module.exports = Monitor
