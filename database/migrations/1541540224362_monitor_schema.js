'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MonitorSchema extends Schema {
  up () {
    this.create('monitors', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.string('name', 60).notNullable()
      table.string('type').notNullable()
      table.string('url').notNullable()
      table.json('query')
      table.boolean('is_active')
      table.timestamps()
    })
  }

  down () {
    this.drop('monitors')
  }
}

module.exports = MonitorSchema
