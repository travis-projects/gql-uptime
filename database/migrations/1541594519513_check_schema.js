'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CheckSchema extends Schema {
  up () {
    this.create('checks', (table) => {
      table.increments()
      table.integer('monitor_id').unsigned().references('id').inTable('monitors')
      table.string('status').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('checks')
  }
}

module.exports = CheckSchema
